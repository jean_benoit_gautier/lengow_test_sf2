<?php

namespace TestBundle\Services;

use Symfony\Component\HttpKernel\Log\LoggerInterface;

class LengowTest
{
    protected $logger;
    protected $url_orders;

    public function __construct($logger, $url_orders)
    {
        $this->logger = $logger;
        $this->url_orders = $url_orders;
    }

    public function getOrders()
    {
        $error = $logger->error('Erreur lors du téléchargement du fichier xml');
        $call = $this->logger->info('Appel du fichier xml');
        $xml = file_get_contents($this->url_orders);
        $back = $this->logger->info('Retour du fichier xml');

        return ['error' => $error,
                'call' => $call,
                'xml' => $xml,
                'back' => $back, ];
    }
}
