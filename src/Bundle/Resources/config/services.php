<?php

use Symfony\Component\DependencyInjection\Definition;

$container->setParameter('url_orders', 'http://test.lengow.io/orders-test.xml');

$container->setDefinition(
    'service.lengow_test',
    new Definition(
        'TestBundle\Services\lengow_test',
        ['@logger', '%url_orders%']
    )
);
